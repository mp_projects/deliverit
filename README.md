# **DeliverIT**
### **Freight Forwarding Management System**

## **DESCRIPTION**

### **System description**

“DeliverIT” is an application progarming interface (API) for managing http-requests to the SQL database of the system.

Its purpouse is to manage the generic proces of creating parcels in a warehouse, organizing them in shipments and sending the shipments to another warehouses.

The system allows three types of users:
-	anonimous - any unregisyered user;
-	customer - a user registered with his email;
-	employee - a user registered with his email.

Each user has differnt level of access to the database resources.

Anonimous users can:
-	get an address-list of all company warehouses;
-	get the count of all company clients (the customers);
-	register themselves in order to become cistomers. 

Customers can:
-	get lsts of their parcels filtered by status;
-	check the status of their parcel;

Employees can:
-	register another employee;
-	create parcels and shipments in the system;
-	add parcels to a shipment and remove parcels from a shipment;
-	change the state (update) parcels and shipments;
-	get lists of all parcels, shipments, cities and countryes as well as specific filters of parcels and shipments.


### **About creating and modifying items**

**Employees and Customers**\
In order to register employees and customers must have: first name; last name; address; email. These can be changed later.

**Parcels**\
At creating a parcel is required to have at least: purchaser (a customer), weight, category (electronics, clothing, medical etc.) and the warehouse it is created in. Optional property that need to be specified later is the shipment it will be sent with. Each property could be changed later.

After inclusion in any shipment the parcel's warehouse automatically gets the destination warehouse of the shipment.

**Shipments**\
At creating a shipment is required to have at least warehouse to wich it will be sent (destination warehouse). Optional properties that need to be specified later are departure and arrival dates. Each property could be changed later.

Shipments could have status "reparing", "on the way" and "completed". At creation the shipment gets automatically initial status "preparing". The shipment status could be changed to "on the way" only if it contains any parcels.

Parcels can be added to and removed from a shipment that has status "preparing".


### **Functional description**

Currently this API supports the next operations through the basic http-request methods GET, POST, PUT and DELETE:
-	cities - read;
-	countries - read;
-	warehouses - read;
-	parcels - create, read, update, delete;
-	shipments - create, read, update, delete;
-	customers - create, read, update, delete;
-	employees - create, read, update, delete;


## **INSTRUCTIONS FOR BUILDING AND RUNNING THE PROJECT**

1.	Download folder DeliverIT.
2.	Run the file DeliverIT.sln in Visual Studio.
3.	In menu Tools select NuGet Packager Manager then Package Manager Console (PMC).
4.	In PMC type the commad "update-database" to build the database on SQL Server and load it with initial data. Use server name .\SQLEXPRESS when connecting to server.
5.	Run the project - DeliverIT.Web.
6.	Http-requests can be made in [Swagger](http://localhost:5000/swagger/index.html) which will run automatically after running the project.


## **DATABASE DIAGRAM**

Check our [database diagram](https://gitlab.com/JoroStef/DeliverIT/-/blob/master/DeliverIT/Database_Diagram.png)


## **PLANS FOR FUTURE DEVELOPEMENT**

Having the customer address the system should be able to send its parcels to the warehouse that is closest to him.


## **OTHER LINKS**

Visit our [Trello board]( https://trello.com/b/k40tua3B/deliverit-api)

