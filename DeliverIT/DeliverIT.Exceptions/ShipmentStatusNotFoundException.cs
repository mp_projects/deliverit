﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class ShipmentStatusNotFoundException : Exception
    {
        public ShipmentStatusNotFoundException(int id)
            : base(string.Format(ErrorMessages.NOT_FOUND, "Shipment status", "id", id))
        {

        }

        public ShipmentStatusNotFoundException(string name)
            : base(string.Format(ErrorMessages.NOT_FOUND, "Shipment status", "value", name))
        {

        }
    }
}
