﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class CountryNotFoundException : Exception
    {
        public CountryNotFoundException(int id) 
            : base(string.Format(ErrorMessages.NOT_FOUND,"Country", "id", id))
        {

        }

        public CountryNotFoundException(string name)
            : base(string.Format(ErrorMessages.NOT_FOUND, "Country", "name", name))
        {

        }
    }
}
