﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class CustomerNotFoundException : Exception
    {
        public CustomerNotFoundException(int id)
            : base(string.Format(ErrorMessages.NOT_FOUND,"Customer", "id", id))
        {

        }

        public CustomerNotFoundException(string message)
            : base(message)
        {

        }
    }
}
