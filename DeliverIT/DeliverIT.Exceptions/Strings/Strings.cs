﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions.Strings
{
    public static class Strings
    {
        public const string TEST_DB = "TestDatabase";
        public const string PARCEL_ADDED = "Parcel successfully added.";
        public const string PARCEL_REMOVED = "Parcel successfully removed.";
    }
}
