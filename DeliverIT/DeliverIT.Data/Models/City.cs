﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DeliverIT.Data.Models
{
    /// <author>
    /// Georgi Stefanov
    /// </author>
    public class City
    {
        [Key]
        public int CityId { get; set; }

        [Required(ErrorMessage = ErrorMessages.CITY_NAME_REQUIRED)]
        [StringLength(30, MinimumLength = 1, ErrorMessage = "{0} should be up to {2} and {1} symbols long")]
        public string Name { get; set; }

        [Required(ErrorMessage = ErrorMessages.COUNTRY_ID_REQUIRED)]
        public int CountryId { get; set; }

        public Country Country { get; set; }
    }
}
