﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeliverIT.Data.Models
{
    /// <author>
    /// Georgi Stefanov
    /// </author>
    public class Warehouse
    {
        public Warehouse()
        {
            Employees = new List<Employee>();
        }

        [Key]
        public int WarehouseId { get; set; }

        [ForeignKey("AddressId")]
        [Required(ErrorMessage = ErrorMessages.ADDRESS_ID_REQUIRED)]
        public int AddressId { get; set; }

        public Address Address { get; set; }

        public ICollection<Employee> Employees { get; set; }
    }
}
