﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DeliverIT.Data.Models
{
    public class Category
    {
        public Category()
        {
            this.Parcels = new List<Parcel>();
        }

        [Key]
        public int CategoryId { get; set; }

        [Required(ErrorMessage = ErrorMessages.CATEGORY_NAME_REQUIRED)]
        public string Name { get; set; }

        public ICollection<Parcel> Parcels { get; set; }
    }
}
