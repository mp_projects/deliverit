﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeliverIT.Data.Models
{
    /// <author>
    /// Mihail Popov
    /// </author>
    public class Parcel
    {
        [Key]
        public int ParcelId { get; set; }

        [ForeignKey("CustomerId")]
        [Required(ErrorMessage = ErrorMessages.CUSTOMER_ID_REQUIRED)]
        public int CustomerId { get; set; }

        public Customer Purchaser { get; set; }

        public int? WarehouseId { get; set; }

        public Warehouse Warehouse { get; set; }

        [Required(ErrorMessage = ErrorMessages.WEIGHT_REQUIRED)]
        public double Weight { get; set; }

        [ForeignKey("CategoryId")]
        [Required(ErrorMessage = ErrorMessages.CATEGORY_ID_REQUIRED)]
        public int CategoryId { get; set; }

        public Category Category { get; set; }

        public int? ShipmentId { get; set; }

        public Shipment Shipment { get; set; }
    }
}
