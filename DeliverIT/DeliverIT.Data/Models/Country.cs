﻿using DeliverIT.Exceptions.Messages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DeliverIT.Data.Models
{
    /// <author>
    /// Mihail Popov
    /// </author>
    public class Country
    {
        [Key]
        public int CountryId { get; set; }

        [Required(ErrorMessage = ErrorMessages.COUTNRY_NAME_REQUIRED)]
        [StringLength(30, MinimumLength = 1, ErrorMessage = "{0} should be between {2} and {1} symbols long")]
        public string Name { get; set; }

        public IEnumerable<City> Cities { get; set; } = new List<City>();
    }
}
