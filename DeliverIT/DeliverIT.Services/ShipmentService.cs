﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliverIT.Exceptions;
using DeliverIT.Services.Models;
using Microsoft.EntityFrameworkCore;
using DeliverIT.Services.Models.OutputDTOs;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Exceptions.Messages;
using DeliverIT.Services.QueryObjects;

namespace DeliverIT.Services
{
    /// <summary>
    /// Shipment Service
    /// </summary>
    /// <seealso cref="DeliverIT.Services.Interfaces.IShipmentService" />
    public class ShipmentService : IShipmentService
    {
        /// <summary>
        /// The database context
        /// </summary>
        private readonly IDeliverITDbContext dbContext;
        /// <summary>
        /// The warehouse service
        /// </summary>
        private readonly IWarehouseService warehouseService;
        private readonly ICustomerService customerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShipmentService"/> class.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        /// <param name="warehouseService">The warehouse service.</param>
        public ShipmentService(IDeliverITDbContext dbContext, IWarehouseService warehouseService, ICustomerService customerService)
        {
            this.dbContext = dbContext;
            this.warehouseService = warehouseService;
            this.customerService = customerService;
        }

        /// <summary>
        /// Creates the specified shipment.
        /// </summary>
        /// <param name="shipment">The shipment.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public ShipmentDTO Create(InputShipment shipment)
        {
            if (shipment == null)
            {
                throw new ArgumentException(string.Format(ErrorMessages.FAILED_REGISTRATION, "model"));
            }

            Shipment shipmentToAdd = new Shipment();

            this.SetState(shipmentToAdd, shipment);

            dbContext.Shipments.Add(shipmentToAdd);
            dbContext.SaveChanges();

            var addedShipment = ShipmentsLoadedRealtions()
                .FirstOrDefault(sh => sh.ShipmentId == shipmentToAdd.ShipmentId);

            ShipmentDTO shipmentDTO = ShipmentToDTO(addedShipment);

            return shipmentDTO;
        }

        /// <summary>
        /// Gets the specified shipment.
        /// </summary>
        /// <param name="id">The shipment identifier.</param>
        /// <returns></returns>
        /// <exception cref="ShipmentNotFoundException"></exception>
        public ShipmentDTO Get(int id)
        {

            var shipment = ShipmentsLoadedRealtions()
                .FirstOrDefault(sh => sh.ShipmentId == id)
                ?? throw new ShipmentNotFoundException(id);

            ShipmentDTO shipmentDTO = ShipmentToDTO(shipment);

            return shipmentDTO;
        }

        /// <summary>
        /// Gets all shipments.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ShipmentDTO> GetAll()
        {
            var shipments = ShipmentsLoadedRealtions();

            var allShipmentsDTO = ShipmentsToDTO(shipments);

            return allShipmentsDTO;
        }

        /// <summary>
        /// Updates the specified shipment.
        /// </summary>
        /// <param name="id">The shipment identifier.</param>
        /// <param name="updateShipment">The update shipment.</param>
        /// <returns></returns>
        /// <exception cref="ShipmentNotFoundException"></exception>
        public ShipmentDTO Update(int id, InputShipment updateShipment)
        {
            var shipment = dbContext.Shipments.FirstOrDefault(sh => sh.ShipmentId == id)
                ?? throw new ShipmentNotFoundException(id);

            UpdateState(shipment, updateShipment);

            dbContext.SaveChanges();

            var updatedShipment = ShipmentsLoadedRealtions()
                .FirstOrDefault(sh => sh.ShipmentId == shipment.ShipmentId);

            WarehouseDTO warehouseDTO = new WarehouseDTO(updatedShipment.Warehouse);

            var parcels = ParcelsLoadedRelations()
                .Where(p => p.ShipmentId == shipment.ShipmentId)
                .Select(p => new ParcelDTO(p))
                .ToList();

            ShipmentDTO shipmentDTO = new ShipmentDTO(shipment, warehouseDTO, parcels);

            return shipmentDTO;
        }

        /// <summary>
        /// Deletes the specified shipment.
        /// </summary>
        /// <param name="id">The shipment identifier.</param>
        /// <returns></returns>
        /// <exception cref="ShipmentNotFoundException"></exception>
        public bool Delete(int id)
        {
            var shipment = dbContext.Shipments.FirstOrDefault(sh => sh.ShipmentId == id)
                ?? throw new ShipmentNotFoundException(id);

            dbContext.Shipments.Remove(shipment);
            dbContext.SaveChanges();
            return true;
        }

        /// <summary>
        /// Filters the shipments the by warehouse.
        /// </summary>
        /// <param name="warehouseId">The warehouse identifier.</param>
        /// <returns></returns>
        public IEnumerable<ShipmentDTO> FilterByWarehouse(int warehouseId)
        {
            this.warehouseService.Get(warehouseId);

            IEnumerable<Shipment> filteredShipments = ShipmentsLoadedRealtions();

            filteredShipments = filteredShipments
                .Where(sh => sh.Warehouse.WarehouseId == warehouseId);

            var filteredShipmentsDTO = ShipmentsToDTO(filteredShipments);

            return filteredShipmentsDTO;
        }

        public IEnumerable<ShipmentDTO> FilterByCustomer(CustomerFilteringCriterias criterias)
        {
            var customers = customerService.SearchBy(criterias).ToList();
            ICollection<ShipmentDTO> shipmentDTOs = new List<ShipmentDTO>();
            var shipments = ShipmentsLoadedRealtions().ToList();

            foreach(var customer in customers)
            {
                foreach(var shipment in shipments)
                {
                    if(shipment.Parcels.FirstOrDefault(p=>p.CustomerId == customer.Id)!=null)
                    {
                        shipmentDTOs.Add(this.ShipmentToDTO(shipment));
                    }
                }
            }

            return shipmentDTOs;
        }

        /// <summary>
        /// Filters the shipments by warehouse.
        /// </summary>
        /// <param name="criterias">The warehouse search criterias (street/city/country).</param>
        /// <returns></returns>
        public IEnumerable<ShipmentDTO> FilterByWarehouse(ShipmentFilteringCriterias criterias)
        {
            IEnumerable<Shipment> filteredShipments = ShipmentsLoadedRealtions();

            if (criterias.Street != null)
            {
                filteredShipments = filteredShipments.Where(sh => sh.Warehouse.Address.Street == criterias.Street);
            }

            if (criterias.City != null)
            {
                filteredShipments = filteredShipments.Where(sh => sh.Warehouse.Address.City.Name == criterias.City);
            }

            if (criterias.Country != null)
            {
                filteredShipments = filteredShipments.Where(sh => sh.Warehouse.Address.City.Country.Name == criterias.Country);
            }

            var filteredShipmentsDTO = ShipmentsToDTO(filteredShipments);

            return filteredShipmentsDTO;
        }

        /// <summary>
        /// Adds parcel to a shipment.
        /// </summary>
        /// <param name="shipmentId">The shipment identifier.</param>
        /// <param name="parcelId">The parcel identifier.</param>
        /// <exception cref="ParcelNotFoundException"></exception>
        /// <exception cref="ShipmentNotFoundException"></exception>
        /// <exception cref="ArgumentException">
        /// The parcel is already included.
        /// or
        /// The parcel is already in the destination warehouse.
        /// </exception>
        public void AddParcel(int shipmentId, int parcelId)
        {
            var parcel = dbContext.Parcels
                .FirstOrDefault(p => p.ParcelId == parcelId)
                ?? throw new ParcelNotFoundException(parcelId);

            var shipment = dbContext.Shipments
                .FirstOrDefault(s => s.ShipmentId == shipmentId)
                ?? throw new ShipmentNotFoundException(shipmentId);

            if (shipment.ShipmentId == parcel.ShipmentId)
            {
                throw new ArgumentException("The parcel is already included.");
            }

            if (shipment.WarehouseId == parcel.WarehouseId)
            {
                throw new ArgumentException("The parcel is already in the destination warehouse.");
            }

            if (shipment.ShipmentStatusId != 1)
            {
                throw new ArgumentException(string.Format(ErrorMessages.PARCEL_ADD_REMOVE_IMPOSSIBLE, "add"));
            }

            parcel.WarehouseId = shipment.WarehouseId;
            shipment.Parcels.Add(parcel);
            dbContext.SaveChanges();

        }

        /// <summary>
        /// Removes parcel from a shipment.
        /// </summary>
        /// <param name="shipmentId">The shipment identifier.</param>
        /// <param name="parcelId">The parcel identifier.</param>
        /// <returns></returns>
        /// <exception cref="ShipmentNotFoundException"></exception>
        /// <exception cref="ParcelNotFoundException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public bool RemoveParcel(int shipmentId, int parcelId)
        {
            var shipment = dbContext.Shipments
                .Include(sh => sh.Parcels)
                .FirstOrDefault(s => s.ShipmentId == shipmentId)
                ?? throw new ShipmentNotFoundException(shipmentId);

            var parcelToRemove = shipment.Parcels.FirstOrDefault(p => p.ParcelId == parcelId)
                ?? throw new ParcelNotFoundException(parcelId);

            if (shipment.ShipmentStatusId != 1)
            {
                //throw new ArgumentException(@"Can't remove parcel. The shipment is either ""on the way"" or ""completed""!");
                throw new ArgumentException(string.Format(ErrorMessages.PARCEL_ADD_REMOVE_IMPOSSIBLE, "remove"));
            }

            shipment.Parcels.Remove(parcelToRemove);

            if (shipment.Parcels.Count() == 0)
            {
                shipment.ShipmentStatusId = 1;
            }

            dbContext.SaveChanges();

            return true; // why not void??
        }

        /// <summary>
        /// Gets the count of shipments with status "on the way".
        /// </summary>
        /// <returns></returns>
        public int GetOnWayCount()
        {
            int count = dbContext.Shipments.Where(s => s.ShipmentStatus.Status == "on the way").Count();
            return count;
        }

        /// <summary>
        /// Checks the status of a shipment.
        /// </summary>
        /// <param name="shipmentId">The shipment identifier.</param>
        /// <returns></returns>
        /// <exception cref="ShipmentNotFoundException"></exception>
        public string CheckStatus(int shipmentId)
        {
            var shipment = dbContext.Shipments
                .Include(s => s.ShipmentStatus)
                .FirstOrDefault(s => s.ShipmentId == shipmentId)
                ?? throw new ShipmentNotFoundException(shipmentId);

            return shipment.ShipmentStatus.Status;
        }

        public string CheckShipmentStatusByParcel(int parcelId, int customerId)
        {
            string status = null;

            var parcel = dbContext.Parcels
                .Include(p => p.Purchaser)
                .Include(p => p.Shipment)
                    .ThenInclude(sh => sh.ShipmentStatus)
                .FirstOrDefault(p => p.ParcelId == parcelId)
                ?? throw new ParcelNotFoundException(parcelId);

            if (parcel.Purchaser.CustomerId == customerId)
            {
                status = parcel.Shipment.ShipmentStatus.Status;
            }

            return status;
        }

        /// <summary>
        /// Gets the customer parcels from all shipments with the specified status.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        /// <exception cref="CustomerNotFoundException"></exception>
        /// <exception cref="ShipmentStatusNotFoundException"></exception>
        public IEnumerable<ParcelDTO> GetCustomerParcelsByStatus(int customerId, string status)
        {
            _ = dbContext.Customers
                .FirstOrDefault(c => c.CustomerId == customerId)
                ?? throw new CustomerNotFoundException(customerId);

            _ = dbContext.ShipmentStatuses
                .FirstOrDefault(s => s.Status == status)
                ?? throw new ShipmentStatusNotFoundException(status);

            IEnumerable<ParcelDTO> parcelsDTO = dbContext.Shipments
                .Include(s => s.ShipmentStatus)
                .Where(s => s.ShipmentStatus.Status == status)
                .SelectMany(s => s.Parcels)
                .Where(p => p.CustomerId == customerId)
                    .Include(p => p.Category)
                    .Include(p => p.Shipment)
                    .Include(p => p.Warehouse)
                        .ThenInclude(w => w.Address)
                            .ThenInclude(a => a.City)
                                .ThenInclude(c => c.Country)
                .Select(p => new ParcelDTO(p));

            return parcelsDTO;
        }

        //Customers can see the status of a shipment that holds a given parcel


        /// <summary>
        /// Gets the next shipment that will arrive in a warehouse.
        /// </summary>
        /// <param name="warehouseId">The warehouse identifier.</param>
        /// <returns></returns>
        /// <exception cref="WarehouseNotFoundException"></exception>
        /// <exception cref="ShipmentNotFoundException">No next shipment.</exception>
        public ShipmentDTO GetNextByWarehouse(int warehouseId)
        {
            _ = dbContext.Warehouses
                .FirstOrDefault(w => w.WarehouseId == warehouseId)
                ?? throw new WarehouseNotFoundException(warehouseId);

            var shipments = ShipmentsLoadedRealtions();

            var shipment = shipments
                .Where(s => s.WarehouseId == warehouseId)
                .Where(s => s.ShipmentStatusId == 2)
                .OrderBy(s => s.DepartureDate)
                .FirstOrDefault()
            ?? throw new ShipmentNotFoundException("No next shipment.");

            return ShipmentToDTO(shipment);
        }

        /// <summary>
        /// Get all shipments with loaded realtions.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Shipment> ShipmentsLoadedRealtions()
        {
            return dbContext.Shipments
                .Include(sh => sh.ShipmentStatus)
                .Include(sh => sh.Warehouse)
                    .ThenInclude(w => w.Address)
                        .ThenInclude(a => a.City)
                            .ThenInclude(c => c.Country)
                .Include(sh => sh.Parcels);
        }

        /// <summary>
        /// Gets all parcelses with loaded relations.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Parcel> ParcelsLoadedRelations()
        {
            return dbContext.Parcels
                .Include(p => p.Category)
                .Include(p => p.Purchaser)
                    .ThenInclude(c => c.Address)
                        .ThenInclude(a => a.City)
                            .ThenInclude(c => c.Country)
                .Include(p => p.Warehouse)
                    .ThenInclude(c => c.Address)
                        .ThenInclude(a => a.City)
                            .ThenInclude(c => c.Country);
        }


        /// <summary>
        /// Turn a shipment into DTO.
        /// </summary>
        /// <param name="shipment">The shipment.</param>
        /// <returns></returns>
        private ShipmentDTO ShipmentToDTO(Shipment shipment)
        {
            WarehouseDTO warehouseDTO = null;
            if (shipment.WarehouseId != null)
            {
                warehouseDTO = new WarehouseDTO(shipment.Warehouse);
            }

            int id = shipment.ShipmentId;

            var parcels = ParcelsLoadedRelations()
                .Where(p => p.ShipmentId == id)
                .Select(p => new ParcelDTO(p))
                .ToList();

            return new ShipmentDTO(shipment, warehouseDTO, parcels);

        }

        /// <summary>
        /// Turns multiple shipments into DTOs.
        /// </summary>
        /// <param name="shipments">The shipments.</param>
        /// <returns></returns>
        private IEnumerable<ShipmentDTO> ShipmentsToDTO(IEnumerable<Shipment> shipments)
        {
            List<Shipment> shipments2 = shipments.ToList();
            List<ShipmentDTO> shipmentsDTO = new List<ShipmentDTO>();

            foreach (var shipment in shipments2)
            {
                shipmentsDTO.Add(ShipmentToDTO(shipment));
            }

            return shipmentsDTO;
        }

        /// <summary>
        /// Sets the state of a shipment at creation.
        /// </summary>
        /// <param name="shipment">The shipment.</param>
        /// <param name="inputShipment">The input shipment.</param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        private void SetState(Shipment shipment, InputShipment inputShipment)
        {
            if (inputShipment.DepartureDate != null &&
                inputShipment.ArrivalDate != null &&
                inputShipment.DepartureDate >= inputShipment.ArrivalDate)
            {
                throw new ArgumentException(string.Format(ErrorMessages.FAILED_REGISTRATION, "Arrival and Departure dates"));
            }

            if (inputShipment.DepartureDate != null && inputShipment.DepartureDate < DateTime.Now.Date)
            {
                throw new ArgumentException(string.Format(ErrorMessages.FAILED_REGISTRATION, "Departure date"));
            }

            if (inputShipment.ArrivalDate != null && inputShipment.ArrivalDate < DateTime.Now.Date)
            {
                throw new ArgumentException(string.Format(ErrorMessages.FAILED_REGISTRATION, "Arrival date"));
            }

            if (inputShipment.WarehouseId == null)
            {
                throw new ArgumentNullException(string.Format(ErrorMessages.PARAMETER_REQUIRED, "warehoise Id"));
            }

            try
            {
                warehouseService.Get((int)inputShipment.WarehouseId);
            }
            catch (WarehouseNotFoundException)
            {
                throw new ArgumentException(string.Format(ErrorMessages.FAILED_REGISTRATION, "warehouse Id"));
            }

            shipment.DepartureDate = inputShipment.DepartureDate;
            shipment.ArrivalDate = inputShipment.ArrivalDate;
            shipment.ShipmentStatusId = 1;
            shipment.WarehouseId = inputShipment.WarehouseId;
        }

        /// <summary>
        /// Updates the state of a shipment at update.
        /// </summary>
        /// <param name="shipment">The shipment.</param>
        /// <param name="inputShipment">The input shipment.</param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ShipmentStatusException"></exception>
        /// <exception cref="ShipmentStatusNotFoundException"></exception>
        private void UpdateState(Shipment shipment, InputShipment inputShipment)
        {
            DateTime? comapreDeparture = inputShipment.DepartureDate;
            comapreDeparture ??= shipment.DepartureDate;

            DateTime? comapreArrival = inputShipment.ArrivalDate;
            comapreArrival ??= shipment.ArrivalDate;


            if (comapreDeparture != null &&
                comapreArrival != null &&
                comapreDeparture > comapreArrival)
            {
                throw new ArgumentException(string.Format(ErrorMessages.FAILED_UPDATE, "Arrival/Departure dates"));
            }

            if (inputShipment.DepartureDate != null && inputShipment.DepartureDate < DateTime.Now)
            {
                throw new ArgumentException(string.Format(ErrorMessages.FAILED_UPDATE, "Departure date"));
            }

            if (inputShipment.ArrivalDate != null && inputShipment.ArrivalDate < DateTime.Now)
            {
                throw new ArgumentException(string.Format(ErrorMessages.FAILED_UPDATE, "Arrival date"));
            }

            if (inputShipment.WarehouseId != null &&
                dbContext.Warehouses.FirstOrDefault(w => w.WarehouseId == inputShipment.WarehouseId) == null)
            {
                throw new ArgumentException(string.Format(ErrorMessages.FAILED_UPDATE, "warehouse id"));
            }

            var parcelsCount = dbContext.Parcels.Where(p => p.ShipmentId == shipment.ShipmentId).Count();

            if (shipment.ShipmentStatusId == 1 && inputShipment.ShipmentStatusId > 1)
            {
                if (parcelsCount == 0)
                {
                    throw new ShipmentStatusException(string.Format(ErrorMessages.FAILED_UPDATE_2, "No parcels in this shipment. Can not change status from [preparing]."));
                }

                if (shipment.DepartureDate == null || shipment.ArrivalDate == null)
                {
                    throw new ShipmentStatusException(string.Format(ErrorMessages.FAILED_UPDATE_2, "Departure/Arrival date not assigned. Can not change status from [preparing]."));
                }

                if(shipment.WarehouseId == null)
                {
                    throw new ShipmentStatusException(string.Format(ErrorMessages.FAILED_UPDATE_2, "Arrival warehouse not assigned. Can not change status from [preparing]."));
                }
            }

            if (inputShipment.ShipmentStatusId != null)
            {
                _ = dbContext.ShipmentStatuses
                    .FirstOrDefault(s => s.ShipmentStatusId == inputShipment.ShipmentStatusId)
                    ?? throw new ShipmentStatusNotFoundException(inputShipment.ShipmentStatusId.Value);
            }

            if (inputShipment.DepartureDate != null)
            {
                shipment.DepartureDate = inputShipment.DepartureDate;
            }

            if (inputShipment.ArrivalDate != null)
            {
                shipment.ArrivalDate = inputShipment.ArrivalDate;
            }

            if (inputShipment.WarehouseId != null)
            {
                shipment.WarehouseId = inputShipment.WarehouseId;
            }

            if (inputShipment.ShipmentStatusId != null)
            {
                shipment.ShipmentStatusId = inputShipment.ShipmentStatusId.Value;
            }
        }
    }
}
