﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Messages;
using DeliverIT.Services.Interfaces;
using DeliverIT.Services.Models;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Services
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="DeliverIT.Services.Interfaces.IEmployeeService" />
    public class EmployeeService : IEmployeeService
    {
        /// <summary>
        /// The database context
        /// </summary>
        private IDeliverITDbContext dbContext;
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeService"/> class.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        public EmployeeService(IDeliverITDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Gets employee by specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The employee to get as EmployeeDTO object</returns>
        /// <exception cref="EmployeeNotFoundException"></exception>
        public EmployeeDTO Get(int id)
        {
            var employee = GetIncludedEmployees().FirstOrDefault(e => e.EmployeeId == id);

            if (employee == null)
            {
                throw new EmployeeNotFoundException(id);
            }

            return new EmployeeDTO(employee);
        }

        /// <summary>
        /// Gets all employees.
        /// </summary>
        /// <returns>Collection of all employees as EmployeeDTO objects</returns>
        public IEnumerable<EmployeeDTO> GetAll()
        {
            var allEmployees = GetIncludedEmployees();

            return this.GetEmployeeDTOs(allEmployees);
        }

        /// <summary>
        /// Register the specified employee.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <returns>The registered employee as EmployeeDTO object</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public EmployeeDTO Create(InputEmployee employee)
        {
            if (employee == null)
            {
                throw new ArgumentNullException(string.Format(ErrorMessages.FAILED_REGISTRATION, "model"));
            }

            var employeeToAdd = new Employee();

            this.ValidateInput(employee);
            this.SetState(employee, employeeToAdd);

            dbContext.Employees.Add(employeeToAdd);
            dbContext.SaveChanges();

            var lastAddedEmployeeId = dbContext.Employees.Max(e => e.EmployeeId);

            employeeToAdd = this.GetIncludedEmployees().FirstOrDefault(e => e.EmployeeId == lastAddedEmployeeId);

            return new EmployeeDTO(employeeToAdd);
        }

        /// <summary>
        /// Deletes employee by specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>true if the employee is successfully deleted or false otherwise</returns>
        public bool Delete(int id)
        {
            var employee = dbContext.Employees.FirstOrDefault(e => e.EmployeeId == id);

            if (employee == null)
            {
                return false;
            }

            dbContext.Employees.Remove(employee);

            dbContext.SaveChanges();

            return true;
        }

        /// <summary>
        /// Updates customer by specified id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="employee">The employee.</param>
        /// <returns>The updated customer as EmployeeDTO object</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="EmployeeNotFoundException"></exception>
        public EmployeeDTO Update(int id, InputEmployee employee)
        {
            if (employee == null)
            {
                throw new ArgumentNullException(string.Format(ErrorMessages.FAILED_UPDATE, "model"));
            }

            var employeeToUpdate = GetIncludedEmployees().FirstOrDefault(e => e.EmployeeId == id);

            if (employeeToUpdate == null)
            {
                throw new EmployeeNotFoundException(id);
            }

            this.ValidateInput(employee);
            this.UpdateState(employee, employeeToUpdate);

            dbContext.SaveChanges();

            return new EmployeeDTO(employeeToUpdate);
        }

        /// <summary>
        /// Authenticates employee by specified email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>The authenticated employee</returns>
        /// <exception cref="UnauthorizedAccessException">Invalid credentials</exception>
        public Employee AuthenticateByEmail(string email)
        {
            var employee = dbContext.Employees.FirstOrDefault(e => e.Email == email);

            if (employee == null)
            {
                throw new UnauthorizedAccessException(ErrorMessages.INVALID_CREDENTIALS);
            }

            return employee;
        }

        /// <summary>
        /// Gets the employee DTOs.
        /// </summary>
        /// <param name="employees">The employees.</param>
        /// <returns>Collection of the employees as EmployeeDTO objects</returns>
        private IEnumerable<EmployeeDTO> GetEmployeeDTOs(IEnumerable<Employee> employees)
        {
            List<EmployeeDTO> employeeDTOs = new List<EmployeeDTO>();

            foreach (var employee in employees)
            {
                employeeDTOs.Add(new EmployeeDTO(employee));
            }

            return employeeDTOs;
        }

        /// <summary>
        /// Gets the included relations of the employees.
        /// </summary>
        /// <returns>Collection of the employees</returns>
        private IEnumerable<Employee> GetIncludedEmployees()
        {
            var employees = dbContext.Employees
                .Include(e => e.Address)
                   .ThenInclude(a => a.City)
                      .ThenInclude(c => c.Country);

            return employees;
        }

        /// <summary>
        /// Validates the employee input.
        /// </summary>
        /// <param name="inputEmployee">The input employee.</param>
        /// <exception cref="AddressNotFoundException"></exception>
        /// <exception cref="DuplicateEmailAddressException">Employee</exception>
        private void ValidateInput(InputEmployee inputEmployee)
        {
            if (inputEmployee.AddressId != null && dbContext.Employees.FirstOrDefault(c => c.AddressId == inputEmployee.AddressId) == null)
            {
                throw new AddressNotFoundException(inputEmployee.AddressId.Value);
            }

            if (dbContext.Employees.FirstOrDefault(c => c.Email == inputEmployee.Email) != null)
            {
                throw new DuplicateEmailAddressException("Employee", inputEmployee.Email);
            }
        }

        /// <summary>
        /// Sets the customer state.
        /// </summary>
        /// <param name="inputEmployee">The input employee.</param>
        /// <param name="employee">The employee.</param>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        private void SetState(InputEmployee inputEmployee, Employee employee)
        {
            if (inputEmployee.AddressId == null)
                throw new ArgumentNullException(string.Format(ErrorMessages.PARAMETER_REQUIRED, "Address id"));

            if (inputEmployee.Email == null)
                throw new ArgumentNullException(string.Format(ErrorMessages.PARAMETER_REQUIRED, "Email address"));

            if (inputEmployee.FirstName == null)
                throw new ArgumentNullException(string.Format(ErrorMessages.PARAMETER_REQUIRED, "First name"));

            if (inputEmployee.LastName == null)
                throw new ArgumentNullException(string.Format(ErrorMessages.PARAMETER_REQUIRED, "Last name"));

            employee.Email = inputEmployee.Email;
            employee.FirstName = inputEmployee.FirstName;
            employee.AddressId = inputEmployee.AddressId.Value;
            employee.LastName = inputEmployee.LastName;
        }

        /// <summary>
        /// Updates the customer state.
        /// </summary>
        /// <param name="inputEmployee">The input employee.</param>
        /// <param name="employee">The employee.</param>
        public void UpdateState(InputEmployee inputEmployee, Employee employee)
        {
            if (inputEmployee.AddressId != null)
            {
                employee.AddressId = inputEmployee.AddressId.Value;
            }

            if (inputEmployee.Email != null)
            {
                employee.Email = inputEmployee.Email;
            }

            if (inputEmployee.FirstName != null)
            {
                employee.FirstName = inputEmployee.FirstName;
            }

            if (inputEmployee.LastName != null)
            {
                employee.LastName = inputEmployee.LastName;
            }
        }

        /// <summary>
        /// Determines whether the specified employee is authenticated.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>
        ///  true if the employee is authenticated or false otherwise
        /// </returns>
        public bool IsAuthenticated(string email)
        {
            var employee = dbContext.Employees.FirstOrDefault(c => c.Email == email);

            if (employee == null)
            {
                return false;
            }

            return true;
        }
    }
}
