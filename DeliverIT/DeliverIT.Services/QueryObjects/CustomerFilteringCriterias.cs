﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.QueryObjects
{
    public class CustomerFilteringCriterias
    {
        public string Keyword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string  City { get; set; }
        public string Street { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
    }
}
