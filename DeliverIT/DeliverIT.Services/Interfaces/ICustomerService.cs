﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Models;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using DeliverIT.Services.QueryObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Interfaces
{
    public interface ICustomerService
    {
        IEnumerable<CustomerDTO> GetAll();

        CustomerDTO Get(int id);

        CustomerDTO Create(InputCustomer customer);

        CustomerDTO Update(Customer customerToUpdate, InputCustomer customer);

        bool Delete(int id);

        IEnumerable<CustomerDTO> SearchByPartEmail(string email);

        Customer AuthenticateByEmail(string email);

        int GetCountCustomers();

        public bool IsAuthenticated(string email);

        IEnumerable<CustomerDTO> SearchBy(CustomerFilteringCriterias criterias);
    }
}
