﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Services.Models.OutputDTOs
{
    public class CountryCitiesDTO
    {
        public CountryCitiesDTO(Country country)
        {
            this.Cities = country.Cities.Select(c=>c.Name);
        }
        public IEnumerable<string> Cities { get; set; }
    }
}
