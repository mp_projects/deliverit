﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace DeliverIT.Services.Models.OutputDTOs
{
    public class ParcelDTO
    {
        public ParcelDTO(Parcel parcel)
        {
            this.SetParcelDTOState(parcel);
        }

        [JsonIgnore]
        public int Id { get; set; }
        public string ArrivalDate { get; set; }
        public string PurchaserName { get; set; }
        public double Weight { get; set; }
        public string Category { get; set; }
        public string ParcelLocation { get; set; }

        private void SetParcelDTOState(Parcel parcel)
        {
            this.PurchaserName = parcel.Purchaser.FirstName + " " + parcel.Purchaser.LastName;
            this.Weight = parcel.Weight;
            this.Category = parcel.Category.Name;
            this.ParcelLocation = parcel.Warehouse.Address.Street + ", "
                + parcel.Warehouse.Address.City.Name + ", "
                + parcel.Warehouse.Address.City.Country.Name;

            if(parcel.ShipmentId != null)
            {
                this.ArrivalDate = parcel.Shipment.ArrivalDate.Value.ToString("yyyy/MM/dd");
            }

            this.Id = parcel.ParcelId;
        }
    }
}
