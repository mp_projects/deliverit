﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;

namespace DeliverIT.Services.Models.OutputDTOs
{
    public class CountryDTO
    {
        public CountryDTO(Country country)
        {
            this.Name = country.Name;
            this.Id = country.CountryId;
        }

        [JsonIgnore]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
