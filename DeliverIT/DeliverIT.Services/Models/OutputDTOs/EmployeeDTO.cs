﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace DeliverIT.Services.Models.OutputDTOs
{
    public class EmployeeDTO
    {
        public EmployeeDTO(Employee employee)
        {
            this.FullName = employee.FirstName + " " + employee.LastName;

            this.Address = employee.Address.Street +
                ", " + employee.Address.City.Name +
                ", " + employee.Address.City.Country.Name;

            this.Email = employee.Email;
            this.Id = employee.EmployeeId;
        }

        [JsonIgnore]
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        

    }
}
