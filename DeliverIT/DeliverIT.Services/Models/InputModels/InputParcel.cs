﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeliverIT.Services.Models.InputDTOs
{
    public class InputParcel
    {
        public double? Weight { get; set; }

        public int? CategoryId { get; set; }

        public int? WarehouseId { get; set; }

        public int? CustomerId { get; set; }

        public int? ShipmentId { get; set; }
    }
}
