﻿using DeliverIT.Data;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ShipmentsTests
{
    [TestClass]
    public class ShipmentsGetAll_Should
    {
        [TestMethod]
        public void GetAll_ShouldReturnCorrectType()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                // Act
                var shipmentsDb = shipmentService.GetAll().ToList();

                //Assert
                Assert.IsInstanceOfType(shipmentsDb, typeof(IEnumerable<ShipmentDTO>));
            }
        }

        [TestMethod]
        public void GetAll_ShouldReturnSameCollectionAsExpected()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var shipments = context.Shipments.ToList();

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                // Act
                var shipmentsDb = shipmentService.GetAll().ToList();

                //Assert
                Assert.AreEqual(shipments.Count, shipmentsDb.Count);
                Assert.AreEqual(shipments.First().ShipmentId, shipmentsDb.First().Id);
                Assert.AreEqual(shipments.Last().ShipmentId, shipmentsDb.Last().Id);
            }
        }

        [TestMethod]
        public void GetAll_ShouldReturnEmptyCollectionWhenNoShipments()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                var shipments = context.Shipments.ToList();

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                // Act
                var shipmentsDb = shipmentService.GetAll().ToList();

                //Assert
                Assert.AreEqual(shipments.Count, shipmentsDb.Count);
            }
        }

    }
}
