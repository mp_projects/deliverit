﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Tests.ShipmentsTests
{
    [TestClass]
    public class ShipmentGetNextByWarehouse_Should
    {
        [TestMethod]
        public void GetNextByWarehouse_ShouldThrow_WhenInvalidWarehouseId()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                // Act && Assert

                Assert.ThrowsException<WarehouseNotFoundException>(() => shipmentService.GetNextByWarehouse(-1));

            }
        }

        [TestMethod]
        public void GetNextByWarehouse_ShouldThrow_WhenNoNextShipment()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                // Act && Assert

                Assert.ThrowsException<ShipmentNotFoundException>(() => shipmentService.GetNextByWarehouse(1));

            }
        }

        [TestMethod]
        public void GetNextByWarehouse_ShouldReturnCorrectShipment()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                List<Shipment> shipments = new List<Shipment>()
                {
                    new Shipment(){ShipmentStatusId = 2, WarehouseId = 1, DepartureDate = DateTime.Now.AddDays(3) },
                    new Shipment(){ShipmentStatusId = 2, WarehouseId = 1, DepartureDate = DateTime.Now.AddDays(2) },
                    new Shipment(){ShipmentStatusId = 1, WarehouseId = 1, DepartureDate = DateTime.Now.AddDays(1) }
                };

                context.Database.EnsureDeleted();
                context.ShipmentStatuses.AddRange(Util.SeedShipmentStatuses(context));
                context.Cities.AddRange(Util.SeedCities(context));
                context.Countries.AddRange(Util.SeedCountries(context));
                context.Addresses.AddRange(Util.SeedAddresses(context));
                context.Warehouses.AddRange(Util.SeedWarehouses(context));
                context.Shipments.AddRange(shipments);
                context.SaveChanges();

                // Act
                ShipmentDTO nextShipment = shipmentService.GetNextByWarehouse(1);

                // Assert
                Assert.AreEqual(2, nextShipment.Id);
            }
        }

    }
}
