﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DeliverIT.Tests.ShipmentsTests
{
    [TestClass]
    public class ShipmentsGet_Should
    {
        [TestMethod]
        public void GetById_ShouldThrowShipmentNotFoundException_WhenInvalidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                //Act && Assert
                Assert.ThrowsException<ShipmentNotFoundException>(() => shipmentService.Get(10));
            }
        }

        [TestMethod]
        public void GetById_ShouldReturnCorrectShipment_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                //Act
                var shipment = shipmentService.Get(1);

                //Assert
                Assert.IsTrue(1 == shipment.Id);
            }
        }

        [TestMethod]
        public void GetById_ShouldReturnCorrectInstanceType_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                //Act
                var shipment = shipmentService.Get(1);

                //Assert
                Assert.IsInstanceOfType(shipment, typeof(ShipmentDTO));
            }
        }

    }
}
