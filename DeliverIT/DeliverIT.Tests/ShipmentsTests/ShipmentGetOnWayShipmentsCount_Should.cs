﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ShipmentsTests
{
    [TestClass]
    public class ShipmentGetOnWayShipmentsCount_Should
    {
        [TestMethod]
        public void GetOnWayShipmentsCount_ShouldReturnCorrectCount()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                List<Shipment> shipments = new List<Shipment>()
                {
                    new Shipment(){ShipmentStatusId = 2 },
                    new Shipment(){ShipmentStatusId = 2 },
                    new Shipment(){ShipmentStatusId = 1 }
                };
                
                context.Database.EnsureDeleted();
                context.ShipmentStatuses.AddRange(Util.SeedShipmentStatuses(context));
                context.Shipments.AddRange(shipments);
                context.SaveChanges();

                // Act
                int count = shipmentService.GetOnWayCount();

                // Assert
                Assert.AreEqual(2, count);
            }
        }
    }
}
