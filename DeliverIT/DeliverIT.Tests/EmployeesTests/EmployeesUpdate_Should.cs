﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.EmployeesTests
{
    [TestClass]
    public class EmployeesUpdate_Should
    {
        [TestMethod]
        public void Update_ShouldThrowArgumentNullException_WhenNullInputModelIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var id = context.Employees.Max(e => e.EmployeeId);

                //Act && Assert
                Assert.ThrowsException<ArgumentNullException>(() => sut.Update(id, null));
            }
        }

        [TestMethod]
        public void Update_ShouldThrowAddressNotFoundException_WhenInvalidInputModelAddressIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var validId = new Random()
                    .Next(context.Employees.Min(e => e.EmployeeId), context.Employees.Max(e => e.EmployeeId));

                var inputModel = new InputEmployee()
                {
                    AddressId = context.Addresses.Max(a => a.AddressId) + 1,
                    Email = "validemail@gmail.com",
                    FirstName = "validFirstName",
                    LastName = "validLastName"
                };

                //Act && Assert
                Assert.ThrowsException<AddressNotFoundException>(() => sut.Update(validId, inputModel));
            }
        }

        [TestMethod]
        public void Update_ShouldThrowDuplicateEmailAddressException_WhenExistingInputModelEmailIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var validId = new Random()
                    .Next(context.Employees.Min(e => e.EmployeeId), context.Employees.Max(e => e.EmployeeId));

                var inputModel = new InputEmployee()
                {
                    AddressId = context.Employees.Max(e => e.AddressId),
                    Email = "atanaskolev@gmail.com",
                    FirstName = "validFirstName",
                    LastName = "validLastName"
                };

                //Act && Assert
                Assert.ThrowsException<DuplicateEmailAddressException>(() => sut.Update(validId, inputModel));
            }
        }

        [TestMethod]
        public void Update_ShouldThrowEmployeeNotFoundException_WhenInvalidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var invalidId = context.Employees.Max(e => e.EmployeeId) + 1;

                var inputModel = new InputEmployee()
                {
                    AddressId = context.Employees.Max(e => e.AddressId),
                    Email = "validEmail@gmail.com",
                    FirstName = "validFirstName",
                    LastName = "validLastName"
                };

                //Act && Assert
                Assert.ThrowsException<EmployeeNotFoundException>(() => sut.Update(invalidId, inputModel));
            }
        }

        [TestMethod]
        public void Update_ShouldReturnCorrectlyUpdateInstance_WhenValidIdAndinputModelIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var validId = context.Employees.Max(e => e.EmployeeId);

                var inputModel = new InputEmployee()
                {
                    AddressId = context.Employees.Max(e => e.AddressId),
                    Email = "validEmail@gmail.com",
                    FirstName = "validFirstName",
                    LastName = "validLastName"
                };

                var address = context.Addresses.FirstOrDefault(a => a.AddressId == inputModel.AddressId.Value);
                var addressStreet = address.Street;
                var addressCity = address.City.Name;
                var addressCountry = address.City.Country.Name;

                var employee = new Employee()
                {
                    Address = new Address()
                    {
                        AddressId = inputModel.AddressId.Value,
                        Street = addressStreet,
                        City = new City()
                        {
                            Name = addressCity,
                            Country = new Country() { Name = addressCountry }
                        }
                    },
                    FirstName = inputModel.FirstName,
                    LastName = inputModel.LastName,
                    Email = inputModel.Email
                };

                var employeeDTO = new EmployeeDTO(employee);

                var result = sut.Update(validId, inputModel);

                //Act && Assert
                Assert.AreEqual(employeeDTO.Email, result.Email);
                Assert.AreEqual(employeeDTO.FullName, result.FullName);
                Assert.AreEqual(employeeDTO.Address, result.Address);
            }
        }

        [TestMethod]
        public void Update_ShouldReturnCorrectInstanceType_WhenValidIdAndinputModelIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var validId = context.Employees.Max(e => e.EmployeeId);

                var inputModel = new InputEmployee()
                {
                    AddressId = context.Employees.Max(e => e.AddressId),
                    Email = "validEmail@gmail.com",
                    FirstName = "validFirstName",
                    LastName = "validLastName"
                };

                var result = sut.Update(validId, inputModel);

                //Act && Assert
                Assert.IsInstanceOfType(result, typeof(EmployeeDTO));
            }
        }
    }
}
