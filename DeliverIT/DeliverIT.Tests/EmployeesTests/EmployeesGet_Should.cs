﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.EmployeesTests
{
    [TestClass]
    public class EmployeesGet_Should
    {
        [TestMethod]
        public void Get_ShouldThrowEmployeeNotFoundException_WhenInvalidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var invalidId = context.Employees.Max(e => e.EmployeeId) + 1;

                //Act && Assert
                Assert.ThrowsException<EmployeeNotFoundException>(() => sut.Get(invalidId));
            }
        }

        [TestMethod]
        public void Get_ShouldReturnCorrectInstance_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var validId = context.Employees.Max(e => e.EmployeeId);

                //Act
                var result = sut.Get(validId);

                //Assert
                Assert.AreEqual(validId, result.Id);
            }
        }

        [TestMethod]
        public void Get_ShouldReturnCorrectInstanceType_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var validId = context.Employees.Max(e => e.EmployeeId);

                //Act
                var result = sut.Get(validId);

                //Assert
                Assert.IsInstanceOfType(result, typeof(EmployeeDTO));
            }
        }
    }
}
