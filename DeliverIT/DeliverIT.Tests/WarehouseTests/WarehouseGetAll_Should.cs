﻿using DeliverIT.Data;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.WarehouseTests
{
    [TestClass]
    public class WarehouseGetAll_Should
    {
        [TestMethod]
        public void GetAll_ShouldReturnCorrectType()
        {
            //Arrange
            //var options = Util.GetDbContextOptions(nameof(GetAll_ShouldReturnCorrectType));
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new DeliverITDbContext(options))
            {

                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                // Act
                var warehousesDb = warehouseService.GetAll().ToList();

                //Assert
                Assert.IsInstanceOfType(warehousesDb, typeof(IEnumerable<WarehouseDTO>));

                
            }
        }

        [TestMethod]
        public void GetAll_ShouldReturnSameCollectionAsExcepted()
        {
            //Arrange
            //var options = Util.GetDbContextOptions(nameof(GetAll_ShouldReturnSameCollectionAsExcepted));
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new DeliverITDbContext(options))
            {

                Util.SeedDataInMemoryDatabase(context);

                var warehouses = context.Warehouses.ToList();

                var warehouseService = new WarehouseService(context);

                // Act
                var warehousesDb = warehouseService.GetAll().ToList();

                //Assert
                Assert.AreEqual(warehouses.Count, warehousesDb.Count);
                Assert.AreEqual(warehouses.First().WarehouseId, warehousesDb.First().Id);
                Assert.AreEqual(warehouses.Last().WarehouseId, warehousesDb.Last().Id);
            }
        }

    }
}
