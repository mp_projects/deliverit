﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Tests.WarehouseTests
{
    [TestClass]
    public class WarehousesGet_Should
    {
        [TestMethod]
        public void GetById_ShouldThrowWarehouseNotFoundException_WhenInvalidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                //Act && Assert
                Assert.ThrowsException<WarehouseNotFoundException>(() => warehouseService.Get(10));
            }
        }

        [TestMethod]
        public void GetById_ShouldReturnCorrectWarehouse_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                //Act
                var warehouse = warehouseService.Get(1);

                //Assert
                Assert.IsTrue(1 == warehouse.Id);
            }
        }

        [TestMethod]
        public void GetById_ShouldReturnCorrectInstanceType_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                //Act
                var warehouse = warehouseService.Get(1);

                //Assert
                Assert.IsInstanceOfType(warehouse, typeof(WarehouseDTO));
            }
        }

    }
}
