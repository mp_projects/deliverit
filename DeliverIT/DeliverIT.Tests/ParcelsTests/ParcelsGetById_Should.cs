﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ParcelsTests
{
    [TestClass]
    public class ParcelsGetById_Should
    {
        [TestMethod]
        public void GetById_ShouldThrowParcelNotFoundException_WhenInvalidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using(var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var invalidId = context.Parcels.Max(p => p.ParcelId) + 1;

                var sut = new ParcelService(context);

                //Act && Assert
                Assert.ThrowsException<ParcelNotFoundException>(() => sut.Get(invalidId));
            }
        }

        [TestMethod]
        public void GetById_ShouldReturnCorrectInstance_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var validId = new Random().Next(context.Parcels.Min(p => p.ParcelId), context.Parcels.Max(p => p.ParcelId));
                var sut = new ParcelService(context);

                //Act
                var result = sut.Get(validId);

                //Assert
                Assert.AreEqual(validId, result.Id);
            }
        }

        [TestMethod]
        public void GetById_ShouldReturnCorrectInstanceType_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var validId = new Random().Next(context.Parcels.Min(p => p.ParcelId), context.Parcels.Max(p => p.ParcelId));
                var sut = new ParcelService(context);

                //Act
                var result = sut.Get(validId);

                //Assert
                Assert.IsInstanceOfType(result, typeof(ParcelDTO));
            }
        }
    }
}
