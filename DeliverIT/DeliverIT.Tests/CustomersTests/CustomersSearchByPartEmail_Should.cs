﻿using DeliverIT.Data;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.CustomersTests
{
    [TestClass]
    public class CustomersSearchByPartEmail_Should
    {
        [TestMethod]
        public void SearchByPartEmail_ShouldReturnCollectionWithFullOrPartOfEmail_WhenValidPartOfEmailIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);
                var email = "@gmail.com";
                var filteredByPartOfEmail = context.Customers.Where(customer => customer.Email.Contains(email));

                var customerService = new CustomerService(context);

                var customers = customerService.SearchByPartEmail(email);

                Assert.AreEqual(filteredByPartOfEmail.Count(), customers.Count());
                Assert.AreEqual(filteredByPartOfEmail.First().CustomerId, customers.First().Id);
                Assert.AreEqual(filteredByPartOfEmail.Last().CustomerId, customers.Last().Id);
            }
        }

        [TestMethod]
        public void SearchByPartEmail_ShouldReturnEmptyCollection_WhenInvalidEmailIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var invalidEmail = "invalidEmail";

                var sut = new CustomerService(context);

                //Act
                var result = sut.SearchByPartEmail(invalidEmail);

                //Assert
                Assert.IsFalse(result.Any());
            }
        }

        [TestMethod]
        public void SearchByPartEmail_ShouldReturnCorrectCollectionType()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var validPartEmail = "john";

                var sut = new CustomerService(context);

                //Act
                var result = sut.SearchByPartEmail(validPartEmail);

                //Assert
                Assert.IsInstanceOfType(result, typeof(IEnumerable<CustomerDTO>));
            }

        }
    }
}
