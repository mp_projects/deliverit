﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.CustomersTests
{
    [TestClass]
    public class CustomerDelete_Should
    {
        [TestMethod]
        public void Delete_ReturnFalse_WhenInvalidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using(var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var invalidId = context.Customers.Max(c => c.CustomerId) + 1;

                var customerService = new CustomerService(context);
                //Act && Assert
                Assert.IsTrue(false == customerService.Delete(invalidId));
            }
        }

        [TestMethod]
        public void Delete_ShouldSuccessfullyRemoveCustomer_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customersCount = context.Customers.Count();
                var customers = context.Customers.ToList();
                var validId = new Random().Next(customers.Min(c => c.CustomerId), customers.Max(c => c.CustomerId));

                var customerService = new CustomerService(context);

                //Act
                customerService.Delete(validId);

                //Assert
                Assert.AreEqual(context.Customers.Count(), customersCount - 1);
            }
        }
    }
}
