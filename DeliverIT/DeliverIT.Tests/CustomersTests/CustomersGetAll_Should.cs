﻿using DeliverIT.Data;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.CustomersTests
{
    [TestClass]
    public class CustomersGetAll_Should
    {
        [TestMethod]
        public void GetAll_ShouldReturnSameCollectionAsExcepted()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customers = context.Customers.ToList();

                var customerService = new CustomerService(context);

                var customersDb = customerService.GetAll();

                //Act && Assert
                Assert.AreEqual(customers.Count(), customersDb.Count());
                Assert.AreEqual(customers.First().CustomerId, customersDb.First().Id);
                Assert.AreEqual(customers.Last().CustomerId, customersDb.Last().Id);
            }
        }
    }
}
