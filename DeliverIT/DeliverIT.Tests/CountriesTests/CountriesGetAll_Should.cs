﻿using DeliverIT.Data;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.CountriesTests
{
    [TestClass]
    public class CountriesGetAll_Should
    {

        [TestMethod]
        public void GetAll_ShouldReturnSameCollectionAsExpected()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var allCountries = context.Countries.ToList();

                var countryService = new CountryService(context);

                var methodAllCountries = countryService.GetAll();

                //Act && Assert
                Assert.AreEqual(allCountries.Count(), methodAllCountries.Count());
                Assert.AreEqual(allCountries.First().CountryId, methodAllCountries.First().Id);
                Assert.AreEqual(allCountries.Last().CountryId, methodAllCountries.Last().Id);
            }
        }
    }
}
