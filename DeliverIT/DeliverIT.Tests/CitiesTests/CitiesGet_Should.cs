﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Tests.CitiesTests
{
    [TestClass]
    public class CitiesGet_Should
    {
        [TestMethod]
        public void GetById_ShouldThrowCityNotFoundException_WhenInvalidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(nameof(GetById_ShouldThrowCityNotFoundException_WhenInvalidIdIsPassed));

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var cityService = new CityService(context);

                //Act && Assert
                Assert.ThrowsException<CityNotFoundException>(() => cityService.Get(10));
            }
        }

        [TestMethod]
        public void GetById_ShouldReturnCorrectCity_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(nameof(GetById_ShouldReturnCorrectCity_WhenValidIdIsPassed));

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var cityService = new CityService(context);

                //Act
                var city = cityService.Get(1);

                //Assert
                Assert.IsTrue(1 == city.Id);
            }
        }

        [TestMethod]
        public void GetById_ShouldReturnCorrectInstanceType_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(nameof(GetById_ShouldReturnCorrectInstanceType_WhenValidIdIsPassed));

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var cityService = new CityService(context);

                //Act
                var city = cityService.Get(1);

                //Assert
                Assert.IsInstanceOfType(city, typeof(CityDTO));
            }
        }
    }
}
