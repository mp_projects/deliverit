﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIT.Web.Helpers
{
    public class AuthHelper : IAuthHelper
    {
        private readonly ICustomerService customerService;
        private readonly IEmployeeService employeeService;

        public AuthHelper(ICustomerService customerService, IEmployeeService employeeService)
        {
            this.customerService = customerService;
            this.employeeService = employeeService;
        }
        public Customer TryGetCustomer(string email)
        {
           return customerService.AuthenticateByEmail(email);
        }

        public Employee TryGetEmployee(string email)
        {
            return employeeService.AuthenticateByEmail(email);
        }
    }
}
