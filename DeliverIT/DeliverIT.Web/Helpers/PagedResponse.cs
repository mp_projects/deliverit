﻿using DeliverIT.Services.QueryObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIT.Web.Helpers
{
    public class PagedResponse<T>
    {
        public IEnumerable<T> GetPaginated(IEnumerable<T> collection, int pageNumber, int pageSize)
        {
            collection = collection
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);

            return collection;
        }
    }
}
