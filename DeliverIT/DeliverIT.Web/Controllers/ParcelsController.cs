﻿using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Messages;
using DeliverIT.Services.Interfaces;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using DeliverIT.Services.QueryObjects;
using DeliverIT.Web.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIT.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("api/[controller]")]
    [ApiController]
    public class ParcelsController : ControllerBase
    {
        /// <summary>
        /// The parcel service
        /// </summary>
        private readonly IParcelService parcelService;
        /// <summary>
        /// The authentication helper
        /// </summary>
        private readonly IAuthHelper authHelper;
        private readonly ICustomerService customerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ParcelsController"/> class.
        /// </summary>
        /// <param name="parcelService">The parcel service.</param>
        /// <param name="authHelper">The authentication helper.</param>
        public ParcelsController(IParcelService parcelService, IAuthHelper authHelper, ICustomerService customerService)
        {
            this.parcelService = parcelService;
            this.authHelper = authHelper;
            this.customerService = customerService;
        }

        /// <summary>
        /// Gets parcel by specified id.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult Get([FromHeader] string authentication, int id)
        {
            try
            {
                if(customerService.IsAuthenticated(authentication))
                {
                     return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var parcel = parcelService.Get(id);
                return Ok(parcel);
            }
            catch (ParcelNotFoundException pnfe)
            {
                return NotFound(pnfe.Message);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Register the specified parcel.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="parcel">The parcel to register.</param>
        /// <returns></returns>
        [HttpPost("{id}")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult Post([FromHeader] string authentication, [FromBody] InputParcel parcel)
        {
            try
            {
                if(!customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var createdParcel = parcelService.Create(parcel);
                return Created("uri", createdParcel);
            }
            catch (ArgumentNullException ane)
            {
                return BadRequest(ane.Message);
            }
            catch(ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
            catch(CustomerNotFoundException cnfe)
            {
                return BadRequest(cnfe.Message);
            }
            catch(WarehouseNotFoundException wnfe)
            {
                return BadRequest(wnfe.Message);
            }
            catch(CategoryNotFoundException cnfe)
            {
                return BadRequest(cnfe.Message);
            }
            catch(ShipmentNotFoundException snfe)
            {
                return BadRequest(snfe.Message);
            }
        }

        /// <summary>
        /// Updates parcel by specified identifier.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="parcel">The parcel.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult Put([FromHeader] string authentication, int id, [FromBody] InputParcel parcel)
        {
            try
            {
                if(customerService.IsAuthenticated(authentication))
                {
                     return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var updateParcel = parcelService.Update(id, parcel);
                return Ok(updateParcel);
            }
            catch(ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch (ParcelNotFoundException pnfe)
            {
                return NotFound(pnfe.Message);
            }
            catch (CustomerNotFoundException cnfe)
            {
                return BadRequest(cnfe.Message);
            }
            catch (WarehouseNotFoundException wnfe)
            {
                return BadRequest(wnfe.Message);
            }
            catch (CategoryNotFoundException cnfe)
            {
                return BadRequest(cnfe.Message);
            }
            catch (ShipmentNotFoundException snfe)
            {
                return BadRequest(snfe.Message);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Deletes parcel by specified identifier.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult Delete([FromHeader] string authentication, int id)
        {
            try
            {
                if(customerService.IsAuthenticated(authentication))
                {
                     return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var isDeleted = parcelService.Delete(id);

                if (isDeleted)
                {
                    return NoContent();
                }

                return NotFound();
            }
            catch(UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Gets parcels filtered by specified customer criterias.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="firstName">The first name of the customer.</param>
        /// <param name="lastName">The last name of the customer.</param>
        /// <param name="filter">The pagination filter</param>
        /// <returns></returns>
        [HttpGet("customers")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult Get([FromHeader] string authentication, [FromQuery] string firstName, string lastName, [FromQuery] PaginationFilter filter)
        {
            try
            {

                if(customerService.IsAuthenticated(authentication))
                {
                     return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var parcels = parcelService.FilterByCustomer(firstName, lastName);

                if(!parcels.Any())
                {
                    return NoContent();
                }

                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                return Ok(new PagedResponse<ParcelDTO>().GetPaginated(parcels, validFilter.PageNumber, validFilter.PageSize));
            }
            catch(ArgumentException ae)
            {
                return NotFound(ae.Message);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Gets parcels by specified category criterias.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="type">The category type.</param>
        /// <param name="filter">The pagination filter</param>
        /// <returns></returns>
        [HttpGet("categories")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult GetByCategory([FromHeader] string authentication, [FromQuery] string type, [FromQuery] PaginationFilter filter)
        {
            try
            {

                if (customerService.IsAuthenticated(authentication))
                {
                     return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var parcels = parcelService.FilterByCategory(type);

                if(!parcels.Any())
                {
                    return NoContent();
                }

                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                return Ok(new PagedResponse<ParcelDTO>().GetPaginated(parcels, validFilter.PageNumber, validFilter.PageSize));
            }
            catch(ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch(CategoryNotFoundException cnfe)
            {
                return NotFound(cnfe.Message);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Gets parcels filtered by weight.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="from">The starting weight</param>
        /// <param name="to">The ending weight</param>
        /// <param name="filter">The pagination filter</param>
        /// <returns></returns>
        [HttpGet("weight")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult GetByWeight([FromHeader] string authentication, [FromQuery] double from, double to, [FromQuery] PaginationFilter filter)
        {
            try
            {

                if(customerService.IsAuthenticated(authentication))
                {
                     return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var parcels = parcelService.FilterByWeight(to, from);

                if(!parcels.Any())
                {
                    return NoContent();
                }

                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                return Ok(new PagedResponse<ParcelDTO>().GetPaginated(parcels, validFilter.PageNumber, validFilter.PageSize));
            }
            catch(ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }


        /// <summary>
        /// Gets parcels filtered by warehouse criterias.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="city">The warehouse city.</param>
        /// <param name="country">The warehouse country.</param>
        /// <param name="filter">The pagination filter</param>
        /// <returns></returns>
        [HttpGet("warehouses")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult GetByWarehouse([FromHeader] string authentication, [FromQuery] string city, string country, [FromQuery] PaginationFilter filter)
        {
            try
            {

                if(customerService.IsAuthenticated(authentication))
                {
                     return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var parcels = parcelService.FilterByWarehouse(city, country);

                if(!parcels.Any())
                {
                    return NoContent();
                }

                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                return Ok(new PagedResponse<ParcelDTO>().GetPaginated(parcels, validFilter.PageNumber, validFilter.PageSize));
            }
            catch(ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Gets parcels sorted by sorting criterias.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="filter">The pagination filter</param>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult GetSorted([FromHeader] string authentication, [FromQuery] string sortBy, [FromQuery] PaginationFilter filter)
        {
            try
            {

                if(customerService.IsAuthenticated(authentication))
                {
                     return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var sorted = parcelService.SortBy(sortBy);

                if(!sorted.Any())
                {
                    return NoContent();
                }

                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                return Ok(new PagedResponse<ParcelDTO>().GetPaginated(sorted, validFilter.PageNumber, validFilter.PageSize));
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch (UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }
    }
}
